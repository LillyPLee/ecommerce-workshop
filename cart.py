class CartManager:
    """
    Cart manager
    This class use to store owner's first name, last name and items.
    """
    
    def __init__(self, first_name: str, last_name: str):
        """
        Initialize (Constructor).
        Arguments:
            first_name -- A first name of owner (eg. 'Benjapol', 'Kasidit' )
            last_name -- A last name of owner (eg. 'Worakan', 'Phoncharoen' )
        """
        # TODO: 
        # Create a initialize variable that store first name(String), last name(String) and items(Dict)
        # Your code go here

        self.first_name = first_name
        self.last_name = last_name
        self._items = {}

        #easyeasy, Guang said so.

    def add_item(self, item: str, amount: int):
        """
        Add a item to items.
        Arguments:
            item(String) -- An item as string (eg. 'egg', 'milk').
            amount(int)  -- An amount of item (eg. 3, 2)
        """
        # TODO: 
        # Add item and amount to items and store as Dict
        # (eg. 
        #   case1: if item not exist in items
        #   input: item = 'milk', amount = 5
        #   result {'milk': 5}
        #   case2: if item exist in items
        #   input: item = 'milk', amount = 5
        #   result: add amount to existing item {'milk': 5}
        # )
        # Your code go here
        if item in self._items.items():
            self._items[item] += amount
        else:
            self._items[item] = amount
        
        
        # raise NotImplementedError




    #@staticmethod
    def remove_item(self, item):
        """
        Remove an item from items.
        Arguments:
            item -- An item as string (eg. 'egg', 'milk').
        """
        # TODO: 
        # Remove an item from items. HINT: use pop() (https://www.programiz.com/python-programming/methods/dictionary/pop)
        # (eg. 
        # Initializer: {'milk': 5}
        # Input: 'milk'
        # Result: {}
        # )
        # Your code go here

        self._items.pop(item)

        #raise NotImplementedError

    @property
    def total_amount_items(self) -> int:
        """
        Returns total number of items in cart
        """
        # TODO: 
        # Return the total number of item in items.
        # (eg. 
        # Initializer: {'milk': 5, 'egg': 10}
        # Return 15
        # Your code go here
        
        total = 0
        for value in self._items.values(): 
            total += value
            
    
        return total

        #raise NotImplementedError

    @property
    def owner_name(self) -> dict:
        """
        Return owner's first name and last name in cart as Dict
        """
        # TODO: 
        # Return owner's first name and last name in cart as Dict
        # (eg. 
        # Initializer: first_name = 'Kasidit', last_name = 'Phoncharoen'
        # Return {'first_name': 'Kasidit', 'last_name': 'Phoncharoen'}
        # Your code go here

        return { 'first_name' : self.first_name, 'last_name' : self.last_name}

        # raise NotImplementedError

    @property
    def items(self) -> dict:
        """
        Return items in cart
        """
        # TODO: 
        # Return the items in cart of that items.
        # (eg. 
        # Initializer: {'milk': 5, 'egg': 10}
        # Return {'milk': 5, 'egg': 10}
        # Your code go here

        return self._items

        #raise NotImplementedError

