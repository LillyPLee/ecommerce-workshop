from cart import CartManager

if __name__ == "__main__":
    cart = CartManager('Kasidit', 'Phonchareon')
    print(cart.first_name)
    cart.add_item('milk', 5)
    print(cart._items)
    print(cart.total_amount_items)
    print(cart.owner_name)
    print(cart.items)
